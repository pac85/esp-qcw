import 'package:flutter/material.dart';
import 'BluetoothLib.dart';
import 'dart:async';

class BluetoothConnection extends StatefulWidget {
  BluetoothConnection(BluetoothSerial bt_lib, {Key key, this.title}) : super(key: key){this._bt_lib = bt_lib;}

  final String title;
  BluetoothSerial _bt_lib;

  @override
  _ConnectionState createState() => new _ConnectionState(_bt_lib);
}

class _ConnectionState extends State<BluetoothConnection>
{
  _ConnectionState(BluetoothSerial bt_lib){_bt_lib = bt_lib;}


  bool _is_connecting = false;
  BluetoothSerial _bt_lib;

  List<ListTile> list_tiles = [];
  List<String> devices_name_list;

  void connect_to_device(String device_name)
  {
    if(_is_connecting) return;
    _is_connecting = true;
    int index = devices_name_list.indexOf(device_name);
    _bt_lib.tryConnection(index).then((connected){
      if(connected)
      {
        Navigator.pop(context, true);
      }
      else{
        setState(() {
          list_tiles[index] =  new ListTile(
            leading: const Icon(Icons.bluetooth),
            title: new Text(devices_name_list[index]),
            onTap: (){connect_to_device(devices_name_list[index]);},
          );
        });
        _is_connecting = false;
      }
    });
    setState(() {
      list_tiles[index] = new ListTile(
        leading: const Icon(Icons.bluetooth),
        title: new Text(devices_name_list[index]),
        subtitle: new Text("connecting", style: new TextStyle(fontSize: 10.0, color: Colors.grey)),
        trailing: new CircularProgressIndicator(/*valueColor: new AlwaysStoppedAnimation<Color>(Colors.grey),*/ strokeWidth: 5.0 ),
        onTap: (){},
      );
    });
  }

  void _update_device_list()
  {
    _bt_lib.getPairedDevices().then((devices_name_list){
      list_tiles.clear();
      this.devices_name_list = devices_name_list;
      devices_name_list.forEach((device_name){
        list_tiles.add(
          new ListTile(
            leading: const Icon(Icons.bluetooth),
            title: Text(device_name),
            onTap: (){connect_to_device(device_name);},
          )
        );
      });
      setState((){});
    });

  }

  Future<bool> _onWillPop() async{
    return false;
  }

  @override
  Widget build(BuildContext context) {

    ListView devicesList = new ListView(
      shrinkWrap: true,
      padding: const EdgeInsets.all(20.0),
      children: new List<Widget>.from(list_tiles),
    );

    return new WillPopScope(
      onWillPop: (){Navigator.pop(context, false);},
      child: Scaffold(
        appBar: AppBar(
            title: Text('First Screen'),
          ),
        body: Center(
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Text('Please chose a device  ', style: new TextStyle(fontWeight: FontWeight.bold), textScaleFactor: 2.0,),
                  new RaisedButton(child: new Text('scan'), onPressed: _update_device_list)
                ],
              ),
              new Expanded(child: devicesList,),
              new RaisedButton(child: new Text('cancel'), onPressed: (){Navigator.pop(context, false);}),
            ],
          ),
          ),
        ),
    );
  }
}