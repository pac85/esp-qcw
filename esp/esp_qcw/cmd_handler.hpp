#include <stdio.h>
#include <string.h>
#include "synth.h"

#define MIN_TUP 5
#define MAX_TUP 30

#define MIN_BPS 1
#define MAX_BPS 30

#define SUPPLY_VOLTAGE 320.0f
#define BASE_RAMP 40.0f

float tup, td = 3.0f, base, ramp_ampl = 0.5f; //in ms
int bps = 1; //in hz

void fire_callback()
{
    play_ramp(m_to_u(tup), m_to_u(td), BASE_RAMP/SUPPLY_VOLTAGE, ramp_ampl, 0.0f);
    delay(1000/bps);
}

void play_callback()
{
    
}

void set_callback(char * property, char * value_str)
{
    if(strcmp(property, "ton") == 0)
    {
        tup = atof(value_str);
        tup = clamp(tup, MIN_TUP, MAX_TUP);
    }
    else if(strcmp(property, "bps") == 0)
    {
        bps = atoi(value_str);
        bps = clamp(bps, MIN_BPS, MAX_BPS);
    }
    else if(strcmp(property, "amp") == 0)
    {
        ramp_ampl = atof(value_str);
        ramp_ampl = clamp(ramp_ampl, 0.0f, 1.0f);
    }
}
 
