import 'package:flutter/material.dart';
import "BluetoothConnection.dart";
import 'BluetoothLib.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  final int bps_min = 1, bps_max = 30, ton_min = 5, ton_max = 30;
  int _bps = 1, _ton = 5;
  final double ampl_min = 0.2, ampl_max = 1.0;
  double _ampl = 0.3;
  bool _safety = false, _firing = false, _connected = false;
  double _peak_current = 0.0, _bus_lowest = 0.0, _bus_realtime = 0.0;
  BluetoothSerial bt_serial;

  _MyHomePageState(){bt_serial = new BluetoothSerial();}


  void _connect_to_bluetooth() {
    setState(() {
      Navigator.push(context,
        MaterialPageRoute(builder: (context) => BluetoothConnection(bt_serial)),
      ).then((is_connected){_connected = is_connected;});
    });
  }

  void _toggleSafety(bool swicth_state)
  {
    if(! _connected)
    {
      showDialog<void>(
        context: context,
        barrierDismissible: true, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Not connected', style: new TextStyle(fontWeight: FontWeight.bold), textScaleFactor: 1.2,),
            content: new SingleChildScrollView(
              child: new Text('You are not connected to a compatible bluetooth device, please use the connect floating button or the button below'),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('connect'),
                onPressed: () {
                  Navigator.of(context).pop();
                  _connect_to_bluetooth();
                },
              ),
              new FlatButton(
                child: new Text('ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    else if(swicth_state)
    {

      showDialog<bool>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new AlertDialog(
            title: new Text('Safety warning:', style: new TextStyle(fontWeight: FontWeight.bold), textScaleFactor: 1.2,),
            content: new SingleChildScrollView(
              child: new Text('Are you sure you want to turn the safety on?'),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('ok'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              new FlatButton(
                child: new Text('cancel'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        },
      ).then((bool dial_enable){setState((){_safety = dial_enable;});});

    }
    else
      setState(() {
        _safety = swicth_state;
      });
  }

  void _fire()
  {
    bt_serial.sendFire();
  }

  void _continuos_fire(bool switch_state)
  {

  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(widget.title),
      ),
      body: new Padding(
        padding: new EdgeInsets.all(8.0),
        child:new Column(
          children: <Widget>
          [
            new Text('Interrupter settings:', style: new TextStyle(fontWeight: FontWeight.bold), textScaleFactor: 2.0,),
            //bps slider
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Expanded(
                    child: new Slider(
                      value: _bps.toDouble(),
                      min: bps_min.toDouble(),
                      max: bps_max.toDouble(),
                      onChanged: _connected?(double value) {
                        setState(() {
                          _bps = value.round();
                        });
                      }:null,
                      onChangeEnd: (double value) {
                        bt_serial.sendSet("bps", value.round().toString());
                      },
                    ),
                  ),
                  new Text(
                    'bps ' + _bps.toString() + ' Hz',
                  ),
                ],
              ),
            ),
            //ton slider
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Expanded(
                    child: new Slider(
                      value: _ton.toDouble(),
                      min: ton_min.toDouble(),
                      max: ton_max.toDouble(),
                      onChanged: _connected?(double value) {
                        setState(() {
                          _ton = value.round();
                        });
                      }:null,
                      onChangeEnd: (double value) {
                        bt_serial.sendSet("ton", value.round().toString());
                      },
                    ),
                  ),
                  new Text(
                    'ton ' + _ton.toString() + ' ms',
                  ),
                ],
              ),
            ),
            //amplitude slider
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Expanded(
                    child: new Slider(
                      value: _ampl,
                      min: ampl_min.toDouble(),
                      max: ampl_max.toDouble(),
                      onChanged: _connected?(double value) {
                        setState(() {
                          _ampl = value;
                        });
                      }:null,
                      onChangeEnd: (double value) {
                        bt_serial.sendSet("amp", value.toString());
                      },
                    ),
                  ),
                  new Text(
                    'amp ' + (_ampl*100).round().toString() + '%',
                  ),
                ],
              ),
            ),
            //safety buttons
            new Row(
              children: <Widget>[
                new Text('Safety:', style: new TextStyle(fontWeight: FontWeight.bold), textScaleFactor: 1.2,),
                new Switch(value: _safety, onChanged: _toggleSafety)
              ],
            ),
            //Fire buttons
            new Expanded(
                child: new Row(
                  children: <Widget>[
                    new Expanded(child: new RaisedButton(color:Colors.red, onPressed: _safety?_fire:null, child: Text('FIRE!', style: new TextStyle(fontWeight: FontWeight.bold), textScaleFactor: 0.9,))),
                    new Text(' continuos power'),
                    new Switch(value: _firing, onChanged: _safety?_continuos_fire:null),
                  ],
                )
            ),
            //stats
            new Row ( children: <Widget> [new Text("peak current: ", style: new TextStyle(fontSize: 50.0,color: Colors.grey),), new Text(_peak_current.toString(), style:new TextStyle(fontSize: 50.0)),]),
            new Row ( children: <Widget> [new Text("bus lowest: ",   style: new TextStyle(fontSize: 50.0,color: Colors.grey),), new Text(_bus_lowest.toString(), style: new TextStyle(fontSize: 50.0),), ]),
            new Row ( children: <Widget> [new Text("bus realtime",   style: new TextStyle(fontSize: 50.0,color: Colors.grey),), new Text(_bus_realtime.toString(), style: new TextStyle(fontSize: 50.0),),]),
          ],
        )

      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _connect_to_bluetooth,
        tooltip: 'Increment',
        child: new Icon(Icons.bluetooth),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
