#include <BluetoothSerial.h>

#include "cmds.hpp"
#include "pins.h"
#include "utils.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif



void setup() {
    init_pins();
    Serial.begin(115200);
    SerialBT.begin("pac85Qcw"); //Bluetooth device name
    Serial.println("device available fo rpairing");
}


char btBuff[100];

void loop() {
    //polls a command from the bluetooth stream
    if (SerialBT.available()) {
        if(SerialBT.read() == '@')
        {
            SerialBT.readBytesUntil('%', btBuff, 100);
            Command cmd(btBuff);
            cmd.process();
        }
    }
}
