 
#include <stdio.h>
#include <string.h>

#include <bits/stdc++.h>

using namespace std;

enum CmdType
{
    FIRE,
    SET,
    PLAY,
    NONE
};

void fire_callback()
{
    cout << "boom" << endl;
}

void play_callback()
{
    cout << "play callback" << endl;
}

void set_callback(char * property, char * value_str)
{
    cout << "setting " << property << " to " << value_str << endl;
}

struct Command
{
private:
    bool valid;
    CmdType cmd_type = NONE;
    char * args_str; 
public:
    Command(const char * cmd_str)
    {
        valid = false;
        
        //parses command
        char cmd_type_str[6];
        char checksum_str[5];
        char args_str[50];
        int res = sscanf(cmd_str,  "# %s # / %s %[^/]", checksum_str, cmd_type_str, args_str);
        if(res == 0 || res == EOF) return;//the command is invalid therefore we wxit leaving valid to false
        
        //parses command type
        if(strcmp(cmd_type_str, "fire") == 0)
            cmd_type = FIRE;
        else if(strcmp(cmd_type_str, "play") == 0)
            cmd_type = PLAY;
        else if(strcmp(cmd_type_str, "set") == 0)
            cmd_type = SET;
        
        this->args_str = (char*)malloc(strlen(args_str)+1);
        strcpy(this->args_str, args_str);
        
        valid = true;;
        
    }
    
    void process()
    {
        if(!valid) return;
        switch(cmd_type)
        {
            case FIRE:
                fire_callback();
                
                break;
            case SET:
            {
                //parses arguments string
                char property[10], value_str[10];
                sscanf(args_str, "%s %s", property, value_str);
                
                set_callback(property, value_str);
                
                break;
            }
            case PLAY:
                break;
            default:
                break;
        }
    }
    
    const char * get_cmd_str()
    {
        char command[95], cmd_type_str[6];
        switch(cmd_type)
        {
            case FIRE:
                strcpy(cmd_type_str ,"fire");
                break;
            case SET:
            {
                strcpy(cmd_type_str ,"set");
                break;
            }
            case PLAY:
                strcpy(cmd_type_str, "play");
                break;
            default:
                break;
        }
        snprintf(command, 95, " / %s %s / ", cmd_type_str, args_str);
        
        char checksum_str[5];
        strcpy(checksum_str, "123");
        
        char * final_command = (char*)malloc(strlen(command)+strlen(checksum_str)+2);
        snprintf(final_command, 100, "# %s # %s", checksum_str, command);
        return final_command;
    }
    
    ~Command()
    {
        delete(args_str);
    }
    
};

int main()
{
    Command testc("# 123 # / fire /");
    //cout << testc.get_cmd_str() << endl;
    testc.process();
}
